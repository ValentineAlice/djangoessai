from django.contrib import admin

# Register your models here.

# on ajoute ici pour indiquer qu'un obejt à une interface d'administration


from .models import Entreprise, Societe, Types, Etat, Facture

admin.site.register(Entreprise)
admin.site.register(Societe)
admin.site.register(Types)
admin.site.register(Etat)
