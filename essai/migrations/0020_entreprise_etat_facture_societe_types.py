# Generated by Django 3.2.1 on 2021-05-11 07:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('essai', '0019_auto_20210511_0957'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entreprise',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomEntreprise', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Etat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomEtat', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Types',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomTypes', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Societe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomSociete', models.CharField(max_length=200)),
                ('entreprise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='essai.entreprise')),
            ],
        ),
        migrations.CreateModel(
            name='Facture',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomFacture', models.CharField(max_length=200)),
                ('date', models.DateField()),
                ('etat', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='essai.etat')),
                ('societe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='essai.societe')),
                ('types', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='essai.types')),
            ],
        ),
    ]
