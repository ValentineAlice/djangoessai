# Generated by Django 3.2.1 on 2021-05-07 13:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('essai', '0007_auto_20210507_1458'),
    ]

    operations = [
        migrations.AlterField(
            model_name='societe',
            name='numfacture',
            field=models.IntegerField(default=0),
        ),
    ]
