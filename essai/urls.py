from django.urls import path

from . import views


"""
urls.py nous permet de lier la vue avec une url
path('Donné en arg', view.NomDeLaVue, name='NomDeLaVue'),

"""


urlpatterns = [
    path('', views.index, name='index'),
    path('Cdatetime/', views.Cdatetime, name='Cdatetime'),
    path('main/', views.main, name='main'),
    path('facture/',views.facture,name='facture'),
]

"""
Il faut maintenant indiquer au projet
le fichier de configuration de l'application
(modifier monProjet/urls.py)

"""