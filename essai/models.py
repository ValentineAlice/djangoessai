from django.db import models

# Create your models here.
# si non spe dans la class la pk est un id autofield


class Etat(models.Model):
    # -> En Cours, En attente, Livré
    nomEtat = models.CharField(max_length=200)
    
    def __str__(self):
        return self.nomEtat

class Types(models.Model):
    # -> Pdf, Papier, Mail,... etc
    nomTypes = models.CharField(max_length=200)
    
    def __str__(self):
        return self.nomTypes

class Entreprise(models.Model):
    nomEntreprise = models.CharField(max_length=200)
    
    def __str__(self):
        return self.nomEntreprise

class Societe(models.Model):
    entreprise = models.ForeignKey(Entreprise,on_delete=models.CASCADE)
    nomSociete = models.CharField(max_length=200)
   
    def __str__(self):
        return self.nomSociete

class Facture(models.Model):
    nomFacture =  models.CharField(max_length=200)
    societe = models.ForeignKey(Societe,on_delete=models.CASCADE)
    types = models.ForeignKey(Types,on_delete=models.CASCADE)
    etat = models.ForeignKey(Etat,on_delete=models.CASCADE)
    date = models.DateField()

    def __str__(self):#chaine de str
        return self.nomFacture