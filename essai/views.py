from django.shortcuts import render

from django.http import HttpResponse,Http404
import datetime

from .models import Entreprise, Societe, Types, Etat, Facture
# On crée les vues ici

def index(request):
    """
    Lorsqu’une page est demandée, Django crée un objet HttpRequest
    qui contient des metadonnées, la vue doit lui renvoyer un objet
    HttpResponse 
    
    """
    return HttpResponse("Ceci est un index. <br> Mais aussi la vue la plus simple possible.")
   
    """
    Une fois la vue créé il convient de lassocier à une url
    Grâce à urls.py

    """

def facture(request):

    """
    # pb -> q.attribut , il ne prend pas la valeur de attribut mais son nom
    def lister(Class,attribut):
        class_list =Class.objects.order_by(attribut)
        output = ', '.join([q.attribut for q in class_list])
        res_list = [x for x in output.split(",")]
        return res_list
    """  
   
    #on recup le nom de l'entreprise qui a l'id 1
    nomEntreprise = Entreprise.objects.get(id=1)

    #on recup la liste des societe de cette entreprise
    nomSociete = Societe.objects.filter(entreprise__nomEntreprise__icontains=nomEntreprise)
    ok = list(nomSociete)


    # 1er graphique -> nombre de facture avec type


    # on recup la liste des types
    typesList = Types.objects.order_by('nomTypes')
    
    # on transforme en chaine de str séparé par ,
    output = ', '.join([q.nomTypes for q in typesList])

    # on slit grace , pour en faire une list (categorie = axe des x)
    categories = [x for x in output.split(",")]

    #on compte le nombre de facture par type envoyer en arg
    def nombreFact(x,y):
        return Facture.objects.filter(types__nomTypes__icontains=x).filter(societe__nomSociete__icontains=y).count()

    types=[]
    for x in categories:
        types.append(nombreFact(x.strip(),ok[0]))

    # axes des y + series
    donnee = [('Jouve',types)]

    # 2nd graphique -> categorie = chaque facture par heure avec -> etat
 
    etatList = Etat.objects.order_by('nomEtat')
    out = ', '.join([a.nomEtat for a in etatList])
    etat = [x for x in out.split(",")]

    def nombreFactE(x,y):
        return Facture.objects.filter(etat__nomEtat__icontains=x).filter(societe__nomSociete__icontains=y).count()

    state=[]
    for x in etat:
        state.append(nombreFactE(x.strip(),ok[0]))
        

    resEtat = [('Jouve',state)]

    return render(request, 'essai/facture.html', locals())


# test asynchrone :
async def Cdatetime(request):
    now = datetime.datetime.now()
    html = '<html><body>It is now %s.</body></html>' % now
    return HttpResponse(html)

# test boucle :
# Create your views here.
def main(request):
 
    categories = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    cities = [
    ('Tokyo', [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]),
    ('New York', [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]),
    ('London', [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]),
    ('Berlin', [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1])
    ]
    return render(request, 'essai/template.html', locals())
